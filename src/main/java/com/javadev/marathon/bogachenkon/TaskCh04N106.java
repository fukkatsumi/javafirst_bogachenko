package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh04N106 {
    public static void main(String[] args) {
        int month = readIntFromConsole("Input the number of month: ");
        System.out.println("The season of this month is: " + getSeason(month));

    }

    public static int readIntFromConsole(String inputText) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(inputText);
        return scanner.nextInt();
    }

    public static String getSeason(int aMonthNumber) {
        String season = "";
        switch (aMonthNumber) {
            case 1:
                season = "Winter";
                break;
            case 2:
                season = "Winter";
                break;
            case 3:
                season = "Spring";
                break;
            case 4:
                season = "Spring";
                break;
            case 5:
                season = "Spring";
                break;
            case 6:
                season = "Summer";
                break;
            case 7:
                season = "Summer";
                break;
            case 8:
                season = "Summer";
                break;
            case 9:
                season = "Autumn";
                break;
            case 10:
                season = "Autumn";
                break;
            case 11:
                season = "Autumn";
                break;
            case 12:
                season = "Winter";
                break;
        }
        return season;
    }
}
