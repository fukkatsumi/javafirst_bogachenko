package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh05N010 {
    public static void main(String[] args) {
        double course = readDoubleFromConsole("Введите курс: ");
        showTransferTable(transfering(course));
    }

    public static double readDoubleFromConsole(String inputText) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(inputText);
        return scanner.nextDouble();
    }

    public static void showTransferTable(double[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println((i + 1) + "$ -> " + array[i] + "руб.");
        }
    }

    public static double[] transfering(double course) {
        double[] array = new double[20];
        for (int i = 0; i < 20; i++) {
            array[i] = (i + 1) * course;
        }
        return array;
    }
}
