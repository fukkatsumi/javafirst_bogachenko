package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh04N036 {
    public static void main(String[] args) {
        int time = readIntFromConsole("Введите время, прошедшее с начала очередного часа: ");
        System.out.println("Цвет светофора: " + getTrafficLightColor(time));
    }

    public static int readIntFromConsole(String inputText) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(inputText);
        return scanner.nextInt();
    }

    public static String getTrafficLightColor(int minute) {
        return minute % 5 >= 0 && minute % 5 < 3 ? "green" : "red";
    }
}
