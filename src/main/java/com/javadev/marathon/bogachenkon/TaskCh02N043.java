package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh02N043 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Введите два числа через Enter: ");
        int firstNumber = readIntFromConsole("Enter the first number: ");
        int secondNumber = readIntFromConsole("Enter the second number: ");
        System.out.println("Результат проверки на делимость " + divisibilityCheck(firstNumber, secondNumber));
    }

    public static int readIntFromConsole(String inputText) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(inputText);
        return scanner.nextInt();
    }

    public static int divisibilityCheck(int firstNumber, int secondNumber) {
        return firstNumber % secondNumber * secondNumber % firstNumber + 1;
    }
}
