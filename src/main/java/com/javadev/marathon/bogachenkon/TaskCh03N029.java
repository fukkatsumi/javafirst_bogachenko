package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh03N029 {
    public static void main(String[] args) {
        int x = readIntFromConsole("Введите X: ");
        int y = readIntFromConsole("Введите Y: ");
        int z = readIntFromConsole("Введите Z: ");

        System.out.println("Каждое из чисел X и Y нечетное: " + exerciseA(x, y)
                + "\n Только одно из чисел X и Y меньше 20: " + exerciseB(x, y)
                + "\n Хотя бы одно из чисел X и Y равно нулю: " + exerciseV(x, y)
                + "\n Каждое из чисел X, Y, Z отрицательное: " + exerciseG(x, y, z)
                + "\n Только одно из чисел X, Y и Z кратно пяти: " + exerciseD(x, y, z)
                + "\n Хотя бы одно из чисел X, Y, Z больше 100: " + exerciseE(x, y, z));
    }

    public static int readIntFromConsole(String inputText) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(inputText);
        return scanner.nextInt();
    }

    public static boolean exerciseA(int firstNumber, int secondNumber) {
        return (firstNumber % 2 != 0) && (secondNumber % 2 != 0);
    }

    public static boolean exerciseB(int firstNumber, int secondNumber) {
        return (firstNumber < 20) ^ (secondNumber < 20);
    }

    public static boolean exerciseV(int firstNumber, int secondNumber) {
        return (firstNumber == 0) || (secondNumber == 0);
    }

    public static boolean exerciseG(int firstNumber, int secondNumber, int thirdNumber) {
        return (firstNumber < 0) && (secondNumber < 0) && (thirdNumber < 0);
    }

    public static boolean exerciseD(int firstNumber, int secondNumber, int thirdNumber) {
        return (firstNumber % 5 == 0) ^ (secondNumber % 5 == 0) ^ (thirdNumber % 5 == 0);
    }

    public static boolean exerciseE(int firstNumber, int secondNumber, int thirdNumber) {
        return (firstNumber > 100) || (secondNumber > 100) || (thirdNumber > 100);
    }
}
