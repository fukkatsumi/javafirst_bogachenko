package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh05N064 {
    public static void main(String[] args) {
        int[][] array = fillArray();
        System.out.println("Плотность области: " + getAreaDensity(array));
    }

    public static int readIntFromConsole(String inputText) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(inputText);
        return scanner.nextInt();
    }

    public static int[][] fillArray() {
        int[][] array = new int[2][12];
        for (int i = 0; i < 12; i++) {
            array[0][i] = readIntFromConsole("Введите количество жителей (в тысячах человек) " + (i + 1) + " района: ");
            array[1][i] = readIntFromConsole("Введите площадь (в км2) " + (i + 1) + " района: ");
        }
        return array;
    }

    public static int getAreaDensity(int[][] array) {
        int result = 0;

        for (int i = 0; i < 12; i++) {
            result += array[0][i] * array[1][i];
        }
        return result;
    }
}
