package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh02N039 {
    /**
     * HOUR_FACTOR = 360 / 12 = 30
     * MINUTE_FACTOR = 30 / 60 = 0.5
     * SECOND_FACTOR = 0.5 / 60 = 0.0083
     */
    public static final Double HOUR_FACTOR = 30.0;
    public static final Double MINUTE_FACTOR = 0.5;
    public static final Double SECOND_FACTOR = 0.0083;

    public static void main(String[] args) {
        int hour = readIntFromConsole("Введите час: ");
        int minute = readIntFromConsole("Введите минуту: ");
        int second = readIntFromConsole("Введите секунду: ");

        if (!(hour > 23 || hour <= 0 || minute > 59 || minute <= 0 || second > 59 || second <= 0)) {
            System.out.println("Угол равен: " + getTimeAngel(hour, minute, second));
        } else {
            System.out.println("Время введено некорректно!");
        }
    }

    public static int readIntFromConsole(String inputText) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(inputText);
        return scanner.nextInt();
    }


    public static double getTimeAngel(int hour, int minute, int second) {
        if (hour > 12) {
            hour -= 12;
        }
        return hour * HOUR_FACTOR + minute * MINUTE_FACTOR + second * SECOND_FACTOR;
    }
}
