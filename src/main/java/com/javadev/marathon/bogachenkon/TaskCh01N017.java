package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh01N017 {
    public static void main(String[] args) {
        double firstNumber = readDoubleFromConsole("Enter the first number: ");
        double secondNumber = readDoubleFromConsole("Enter the second number: ");
        double thirdNumber = readDoubleFromConsole("Enter the third number: ");
        double fourthNumber = readDoubleFromConsole("Enter the fourth number: ");

        System.out.println("exerciseO: " + exerciseO(firstNumber)
                + "\n exerciseP: " + exerciseP(firstNumber)
                + "\n exerciseR: " + exerciseR(firstNumber, secondNumber, thirdNumber, fourthNumber)
                + "\n exerciseS: " + exerciseS(firstNumber));
    }

    public static double readDoubleFromConsole(String inputText) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(inputText);
        return scanner.nextDouble();
    }

    public static double exerciseO(double first) {
        return Math.sqrt(1 - Math.pow(Math.sin(first), 2));
    }

    public static double exerciseP(double first) {
        return (Math.sqrt(first + 1) + Math.sqrt(first - 1)) / (2 * Math.sqrt(first));
    }

    public static double exerciseR(double first, double second, double third, double fourth) {
        return 1 / Math.sqrt(second * Math.pow(first, 2) + third * first + fourth);
    }

    public static double exerciseS(double first) {
        return Math.abs(first) + Math.abs(first + 1);
    }
}
