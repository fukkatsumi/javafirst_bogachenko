package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh01N003 {
    public static void main(String[] args) {
        System.out.println("Вы ввели число " + readIntFromConsole("Введите число: "));
    }

    public static int readIntFromConsole(String inputText) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(inputText);
        return scanner.nextInt();
    }
}
