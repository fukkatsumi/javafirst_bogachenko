package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh04N015 {
    public static void main(String[] args) {
        int currentMonth = readIntFromConsole("Введите текущий месяц: ");
        int currentYear = readIntFromConsole("Введите текущий год: ");
        int birthdayMonth = readIntFromConsole("Введите месяц рождения: ");
        int birthdayYear = readIntFromConsole("Введите год рождения: ");

        System.out.println("Возраст человека: " + getPersonAge(currentMonth, currentYear, birthdayMonth, birthdayYear));
    }

    public static int readIntFromConsole(String inputText) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(inputText);
        return scanner.nextInt();
    }

    public static int getPersonAge(int aCurrentMonth, int aCurrentYear, int aBirthdayMonth, int aBirthdayYear) {
        return aCurrentMonth > aBirthdayMonth ? aCurrentYear - aBirthdayYear : --aCurrentYear - aBirthdayYear;
    }
}
