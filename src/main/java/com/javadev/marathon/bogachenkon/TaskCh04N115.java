package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh04N115 {
    public static void main(String[] args) {
        int year = readIntFromConsole("Input the year: ");
        System.out.println("Year " + year + " - " + getAnimal(year) + "," + getColor(year));
    }

    public static int readIntFromConsole(String inputText) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(inputText);
        return scanner.nextInt();
    }

    public static String getColor(int year) {
        String color;
        if (year % 10 == 4 || year % 10 == 5) {
            color = "Green";
        } else if (year % 10 == 6 || year % 10 == 7) {
            color = "Red";
        } else if (year % 10 == 8 || year % 10 == 9) {
            color = "Yellow";
        } else if (year % 10 == 0 || year % 10 == 1) {
            color = "White";
        } else {
            color = "Black";
        }
        return color;
    }

    public static String getAnimal(int year) {
        String animal = "";
        switch (year % 12) {
            case 4:
                animal = "Rat";
                break;
            case 5:
                animal = "Cow";
                break;
            case 6:
                animal = "Tiger";
                break;
            case 7:
                animal = "Rabbit";
                break;
            case 8:
                animal = "Dragon";
                break;
            case 9:
                animal = "Snake";
                break;
            case 10:
                animal = "Horse";
                break;
            case 11:
                animal = "Sheep";
                break;
            case 0:
                animal = "Monkey";
                break;
            case 1:
                animal = "Cock";
                break;
            case 2:
                animal = "Dog";
                break;
            case 3:
                animal = "Pig";
                break;
        }
        return animal;
    }
}
