package com.javadev.marathon.bogachenkon;

public class TaskCh05N038 {
    public static void main(String[] args) {
        System.out.println("Расстояние от дома: " + distanceFromHome(100));
        System.out.println("Общий путь: " + getFullWay(100));
    }

    public static double distanceFromHome(int stageCount) {
        int numberOfIterations = ((stageCount - 1) - (stageCount - 1) % 2) / 2;
        double evenDistance = 0;
        double oddDistance = 0;
        for (int i = 1; i < (numberOfIterations + 1); i++) {
            evenDistance = evenDistance + 1 / (2 * (double) i + 1);
            oddDistance = oddDistance - 1 / (2 * (double) i);
        }
        return (evenDistance + 1 + oddDistance - 0.01) + stageCount % 2 / stageCount;
    }

    public static double getFullWay(int stageCount) {
        double result = 0;
        for (int i = 1; i < stageCount + 1; i++) {
            result = result + 1 / (double) i;
        }
        return result;
    }
}
