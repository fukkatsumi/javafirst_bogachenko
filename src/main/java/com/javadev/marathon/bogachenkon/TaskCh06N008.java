package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

import static java.lang.Math.pow;

public class TaskCh06N008 {
    public static void main(String[] args) {
        int number = readIntFromConsole("Input the number: ");
        showArray(findSmallerNumbers(number));
    }

    public static int readIntFromConsole(String inputText) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(inputText);
        return scanner.nextInt();
    }

    public static void showArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0) {
                System.out.println(array[i]);
            }
        }
    }

    public static int[] findSmallerNumbers(int aNumber) {
        int[] array = new int[aNumber];
        for (int i = 1; i < aNumber; i++) {
            if (pow(i, 2) < aNumber) {
                array[i - 1] = (int) pow(i, 2);
            }
        }
        return array;
    }
}
