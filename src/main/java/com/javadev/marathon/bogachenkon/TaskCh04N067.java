package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh04N067 {
    public static void main(String[] args) {
        int day = readIntFromConsole("Input the day of year: ");
        System.out.println("This day is " + getDayOfWeek(day));
    }

    public static int readIntFromConsole(String inputText) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(inputText);
        return scanner.nextInt();
    }

    public static String getDayOfWeek(int day) {
        return day % 7 >= 1 && day % 7 < 6 ? "Workday" : "Weekend";
    }
}
