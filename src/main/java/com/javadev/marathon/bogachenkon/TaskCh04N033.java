package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh04N033 {
    public static void main(String[] args) {
        int number = readIntFromConsole("Введите число: ");

        System.out.println("Верно ли, что число заканчивается четной цифрой? \n" + parityOfTheLastDigit(takeLastDigit(number)));
        System.out.println("Верно ли, что число заканчивается нечетной цифрой? \n" + oddnessOfTheLastDigit(takeLastDigit(number)));
    }

    public static int readIntFromConsole(String inputText) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(inputText);
        return scanner.nextInt();
    }

    public static int takeLastDigit(int aNumber) {
        return Integer.parseInt(Integer.toString(aNumber).substring(Integer.toString(aNumber).length() - 1));
    }

    public static String parityOfTheLastDigit(int aNumber) {
        return aNumber % 2 == 0 ? "Верно" : "Неверно";
    }

    public static String oddnessOfTheLastDigit(int aNumber) {
        return aNumber % 2 != 0 ? "Верно" : "Неверно";
    }
}

